import sys
import numpy as np
from esda.moran import Moran
from libpysal.weights import W, lat2W

if len(sys.argv) != 4:
	print("ERROR! USE " + sys.argv[0] + " <raster_filename> <n_rows> <n_cols> ")
	sys.exit()

raster_filename = sys.argv[1]
n_rows = int(sys.argv[2])
n_cols = int(sys.argv[3])

raster_type = np.dtype((np.int32, (n_rows, n_cols)))

file_raster = open(raster_filename, "rb")
raster = np.fromfile(file_raster, dtype=raster_type)[0]
file_raster.close()

w = lat2W(n_rows, n_cols, rook=False)
lm = Moran(raster, w, transformation='r', permutations=0)

print("{}\t{}".format(raster_filename, lm.I))

