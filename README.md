# README

Prueba de cálculo de Moran por cada dataset de NASA, tomando un solo raster por cada dataset.

## Valores esperados

 | Dataset | Valor Moran | 
 |--------|--------|
 | CAPE_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.23186619949134327 | 
 | CONVfrac_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.24079051200699572 | 
 | DLWRF_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.1343569885480403 | 
 | DSWRF_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.1466338556879126 | 
 | PEVAP_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.19949550899916424 | 
 | PRES_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.23169861593588814 | 
 | SPFH_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.2821878612937598 | 
 | TMP_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.18773248409706686 | 
 | UGRD_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.4546745112072414 | 
 | VGRD_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin | 0.3741402817011984 | 
