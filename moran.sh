#!/usr/bin/bash

python3 -Wignore moran_raster.py APCP_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py CAPE_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py CONVfrac_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py DLWRF_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py DSWRF_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py PEVAP_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py PRES_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py SPFH_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py TMP_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py UGRD_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224
python3 -Wignore moran_raster.py VGRD_NLDAS_FORA0125_H.A20180101.0000.002.grb.SUB.nc4.bin 464 224